# Docker | NextJS

## How to build

Build the image with the Dockerfile

```sh
docker build -t nextjs-app .
```

## How to run

Run the image with Docker

```sh
docker run -p 3000:3000 nextjs-app
```

OR

Run the image with Docker Compose

```sh
docker-compose up -d
```

Application should be available at [http://localhost:3000](http://localhost:3000)

Modifying the `pages/index.js` file should automatically update and reload the application.
